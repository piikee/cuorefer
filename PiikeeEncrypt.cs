﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Threading;
using System.Collections.Specialized;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Collections;

namespace baiduclick
{
    public class PiikeeEncrypt
    {

        private HttpWebRequest myHttpWebRequest;
        public static string Encrypt(string toEncrypt)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes("bc39cz44pduz9_x81a+4*(bh92f@d=@d"); // 256-AES key
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB; // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.Zeros; // better lang support
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return System.Web.HttpUtility.UrlEncode(Convert.ToBase64String(resultArray, 0, resultArray.Length));
        }

        public void finishAPI(ArrayList idlist)
        {
            string id_str = "";
            int finisherr = 0;

            while (true)
            {
                try
                {
                    foreach (string i in idlist)
                    {
                        id_str = id_str + i + ",";
                    }
                    string ipaddress = "";
                    try
                    {
                        IPHostEntry hostInfo = Dns.GetHostByName("index.piikee.net");
                        IPAddress ip = hostInfo.AddressList[0];
                        ipaddress = ip.ToString();
                    }
                    catch
                    { 
                        IPHostEntry host;
                        host = Dns.GetHostEntry("index.qqxk.net");
                        ipaddress = host.AddressList[0].ToString(); 
                    }
                    id_str = id_str.TrimEnd(',');
                    string query = String.Format("access_token={0}&worklist={1}", "tkqxphtyigw19x77gjhps1zliik1popi", id_str);
                    myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://" + ipaddress + ":9090/b/finishtaobao");
                    myHttpWebRequest.Method = "POST";
                    string postData = "p=" + PiikeeEncrypt.Encrypt(query);

                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    myHttpWebRequest.ContentLength = byteArray.Length;
                    string strResponse = "";
                    myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    Stream newStream = myHttpWebRequest.GetRequestStream();
                    newStream.Write(byteArray, 0, byteArray.Length);//写入参数
                    newStream.Close();
                    HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.GetResponse();
                    StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                    strResponse = sr.ReadToEnd();
                    Form1.log("finish response:" + strResponse); 
                    sr.Close();
                    response.Close(); 
                    break;
                }
                catch
                {
                    finisherr++;
                    Thread.Sleep(3000);
                   

                }
            }

        }

        public class BackInfo
        {
            public BackInfo()
            {

            }
            public string msg { get; set; }
            public String code { get; set; }
            public data data { get; set; }

        }
        public class workList
        {
            public string url { get; set; }
            public string keyword { get; set; }

            public int id { get; set; }

        }
        public class data
        {
            public List<workList> worklist { get; set; }

        }


        public List<PiikeeEncrypt.workList> gettask()
        {
            while (true) { 
            try
            { // Create a HttpWebrequest object to the desired URL. 
                myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://index.piikee.net:9090/b/worktaobao");
                myHttpWebRequest.Method = "POST";
                myHttpWebRequest.Proxy = null;
                string postData = "p=lT5nyIJ3eIYIxiToH%2fmFSQ%3d%3d";// "p=" + PiikeeEncrypt.Encrypt("127001");
                myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                myHttpWebRequest.ContentLength = byteArray.Length;
                string strResponse = "";

                Stream newStream = myHttpWebRequest.GetRequestStream();
                newStream.Write(byteArray, 0, byteArray.Length);//写入参数
                newStream.Close();
                HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                strResponse = sr.ReadToEnd();
                sr.Close();
                response.Close();
                
                PiikeeEncrypt.BackInfo backinfo = (PiikeeEncrypt.BackInfo)JsonConvert.DeserializeObject(strResponse, typeof(PiikeeEncrypt.BackInfo));

                List<PiikeeEncrypt.workList> list = backinfo.data.worklist;

                return list;
            }
            catch (Exception oo)
            {
                Console.WriteLine(oo.ToString());
            }
            }

        }
    }
}
