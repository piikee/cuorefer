﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using System.Net;
using System.Collections;
using System.Runtime.InteropServices;
    
using System.Reflection;
namespace myWeb
{ 
    public partial class MyWeb : Form
    {
        protected bool isapp = false;
        protected string url = "";
        protected string kw = "";
        protected string dj = "";
        protected string time_id = "";
        protected string proxyip = "";
        protected int badproxy = 0;
        string doctitle = "default";
        Random rd = new Random(); 
        private Timer clickt  = new System.Windows.Forms.Timer();
        private Timer timeout = new System.Windows.Forms.Timer();
        bool uncompleted = true ;

        private string uag = "";
        private ExtendedWebBrowser webBrowser1;
     //  private static string defaultUserAgent = null;
        [DllImport("urlmon.dll", CharSet = CharSet.Ansi)]//改UA
         
        private static extern int UrlMkSetSessionOption(int dwOption, string pBuffer, int dwBufferLength, int dwReserved);//改UA
         [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
    static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll", SetLastError = true)]
    static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
    [DllImport("KERNEL32.DLL", EntryPoint = "SetProcessWorkingSetSize", SetLastError = true, CallingConvention = CallingConvention.StdCall)]
    internal static extern bool SetProcessWorkingSetSize(IntPtr pProcess, int dwMinimumWorkingSetSize, int dwMaximumWorkingSetSize);

    [DllImport("KERNEL32.DLL", EntryPoint = "GetCurrentProcess", SetLastError = true, CallingConvention = CallingConvention.StdCall)]
        static extern IntPtr ShellExecute(IntPtr hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, ShowCommands nShowCmd);
        const int URLMON_OPTION_USERAGENT = 0x10000001;//改UA
        
        public MyWeb(string []arg)
        {
            InitializeComponent();


           // this.WindowState = FormWindowState.Minimized;
            try
            { 
                wbinstall();
                url = arg[0].Split('|')[0];
                kw = arg[0].Split('|')[1].Replace( "%s"," ");
                dj = arg[0].Split('|')[2].Replace( "%s"," ");
                time_id = arg[0].Split('|')[3];
                proxyip = arg[0].Split('|')[4];
                IEProxy ieproxy = new IEProxy(proxyip);
                ieproxy.RefreshIESettings();
                //if (rd.Next(0, 4) == 1)
                //{
                //    isapp = true;
                //    url = "http://m.taobao.com";
                //}
                Navigate(url);
               
                timeout.Tick += new EventHandler(this.timeouttick);
                timeout.Interval = 15000;
                timeout.Enabled = true;
                Console.WriteLine(timeout.Interval.ToString()+"'"+timeout.Enabled.ToString());
            }
            catch (Exception o) {Console.WriteLine(o.ToString()); }
        }
         public enum ShowCommands : int
    {
        SW_HIDE = 0,
        SW_SHOWNOrmAL = 1,
        SW_NOrmAL = 1,
        SW_SHOWMINIMIZED = 2,
        SW_SHOWMAXIMIZED = 3,
        SW_MAXIMIZE = 3,
        SW_SHOWNOACTIVATE = 4,
        SW_SHOW = 5,
        SW_MINIMIZE = 6,
        SW_SHOWMINNOACTIVE = 7,
        SW_SHOWNA = 8,
        SW_RESTORE = 9,
        SW_SHOWDEFAULT = 10,
        SW_FORCEMINIMIZE = 11,
        SW_MAX = 11
    } 

        private void Navigate(String address)
        {
            Console.WriteLine("strart navigate to:"+address );
            if (String.IsNullOrEmpty(address)) return;
            if (address.Equals("about:blank")) return;
            if (!address.StartsWith("http://") &&
                !address.StartsWith("https://"))
            {
                address = "http://" + address;
            }
            try
            {
                // webBrowser1.Navigate("http://www.taobao.com/", null, null, "User-Agent:Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko");
             
               int ua=   rd.Next(0,35);
            //   isapp = true;
           //   if(isapp )
             //  address = "http://m.taobao.com";
               switch (ua)
               {
                   case 0: uag = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko"; break;// webBrowser1.Navigate(address, null, null, uag); break;
                   case 1: uag = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"; break;// webBrowser1.Navigate(address, null, null, uag); break;
                   case 2: uag = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36"; break;// webBrowser1.Navigate(address, null, null, uag); break;
                   case 3: uag = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0"; break;// webBrowser1.Navigate(address, null, null, uag); break;
                   case 4: uag = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko"; break;// webBrowser1.Navigate(address, null, null, uag); break;
                   case 5: uag = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)"; break;// webBrowser1.Navigate(address, null, null, uag); break;
                   case 6: uag = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "; break;// webBrowser1.Navigate(address, null, null, uag); break; 
                   case 7: uag ="Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";break;
                   case 8: uag ="Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)";break;
                   case 9: uag ="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.77.4 (KHTML, like Gecko) Version/7.0.5 Safari/537.77.4";break;
                   case 10: uag ="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36";break;
                   case 11: uag = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/4.4.1.2000 Chrome/30.0.1599.101 Safari/537.36";break;
                   case 12: uag ="Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0";break;
                   case 13: uag = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36"; break;
                   case 14: uag ="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36 SE 2.X MetaSr 1.0";break;
                   case 15: uag ="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/4.4.1.5000 Chrome/30.0.1599.101 Safari/537.36";break;
                   case 16: uag ="Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0";break;
                   case 17: uag ="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.154 Safari/537.36";break;
                   case 18: uag ="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36 LBBROWSER";break;
                   case 19: uag = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)";break;
                   case 20: uag ="Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36";break;
                   case 21: uag ="Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/4.4.1.5000 Chrome/30.0.1599.101 Safari/537.36";break;
                   case 22: uag = "Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)"; break;
                   case 23: uag = "Mozilla/5.0 (Windows NT 5.1; zh-CN; rv:1.9.1.3) Gecko/20100101 Firefox/19.0"; break;
                   case 24: uag = "Opera/9.99 (Windows NT 5.1; U; zh-CN) Presto/9.9.9"; break;
                   case 25: uag = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36"; break;
                   case 26: uag = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)"; break; 
                   case 27: uag = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36 SE 2.X MetaSr 1.0"; break;
                   case 28: uag = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)"; break;
                   case 29: uag = "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0"; break;
                   case 30: uag = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2"; break;
                   case 31: uag = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36 LBBROWSER"; break;
                   case 44: uag = "Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"; break;
                   default: uag = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "; break;//webBrowser1.Navigate(address, null, null, uag); break;
               }
             
                ChangeUserAgent(uag);
            
            // webBrowser1.Navigate(address ); 
              
                //while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                //{
                //    Application.DoEvents();
                //}

              //  webBrowser1.DocumentCompleted  += new WebBrowserNavigatedEventHandler(webBrowser1_DocumentCompleted);
                webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);
                try
                { webBrowser1.Navigate("about:blank");
                    webBrowser1.Navigate(address, null, null, uag);
                }
                catch (Exception o) { Console.WriteLine(o.ToString()); }
               Console.WriteLine(uag);
           
            // webBrowser1.Navigate(address);
            }
            catch (Exception o)
            {
                Console.WriteLine("catch"+o.ToString());
                return;
            }
        }

        public static void ChangeUserAgent(string userAgent)
        {
          
            UrlMkSetSessionOption(URLMON_OPTION_USERAGENT, userAgent, userAgent.Length, 0); 
          
        }

        private void wbinstall()
        {
            
            this.webBrowser1 = new ExtendedWebBrowser();
            // this.webBrowser1.Size = new System.Drawing.Size(684, 662); 
          this.webBrowser1.Location = new System.Drawing.Point(0, 0);
      this.Controls.Add(this.webBrowser1);
        this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            webBrowser1.ScriptErrorsSuppressed = true;
            webBrowser1.BeforeNewWindow += new System.EventHandler<myWeb.WebBrowserExtendedNavigatingEventArgs>(extendedWebBrowser1_BeforeNewWindow);
            // webBrowser1.Navigating += new WebBrowserNavigatingEventHandler(Navigating_go);
            this.clickt.Tick += new System.EventHandler(this.clickt_Tick);
      //   webBrowser1.Navigating += new WebBrowserNavigatingEventHandler(webBrowser_Navigating);
            webBrowser1.DocumentTitleChanged += new EventHandler(webBrowser1_DocumentTitleChanged);
      
          //  this.WindowState = FormWindowState.Minimized;


        }

       


        private void webBrowser1_DocumentTitleChanged(object sender, EventArgs e)
        {
            try {
             //   this.WindowState = FormWindowState.Minimized;
            
            }
            catch { }
            doctitle  = webBrowser1.DocumentTitle;
            
        }


        private void extendedWebBrowser1_BeforeNewWindow(object sender, WebBrowserExtendedNavigatingEventArgs e)
        {
            ((ExtendedWebBrowser)sender).Navigate(e.Url);
            e.Cancel = true;

        }




        //private void webBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        //{
            
        //    //Check if the custom user agent is set

        //    if (!isUserAgentSet)
        //    {

        //        //cancel the current request

        //        e.Cancel = true;

        //        this.isUserAgentSet = true;


        //        //Navigate to the desired location using the custom user agent
        //        ChangeUserAgent(uag);
        //        webBrowser1.Navigate(e.Url, e.TargetFrameName, null, uag );

        //    }

        //}

        private void webBrowser1_DocumentCompleted(object sender, EventArgs e)
        {




            if (webBrowser1.ReadyState < WebBrowserReadyState.Complete) { Console.WriteLine(webBrowser1.ReadyState.ToString()); return; }
         
             Console.WriteLine("title is:"+doctitle );
            if (doctitle.IndexOf("淘宝") == -1) {
                if (badproxy < 3)
                {
                    Console.WriteLine(doctitle);
                    webBrowser1.Stop();
                    webBrowser1.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);  
                    Navigate(url); 
                    badproxy++;
                    return;
                }
                else
                {
                    try
                    {
                        using (StreamWriter sw = new StreamWriter("taskok.txt", false))
                        {
                            sw.WriteLine("error");
                        }
                        Console.WriteLine(doctitle + "error3");
                    }
                    catch { }
                    return;

                }   
               
            }
            Console.WriteLine("title ok");
            uncompleted = false;
            try
            {
                
               
                string murl = ((WebBrowser)sender).Url.ToString();
                foreach (HtmlElement archor in this.webBrowser1.Document.Links)
                {
                    archor.SetAttribute("target", "_self");
                }
                foreach (HtmlElement form in this.webBrowser1.Document.Forms)
                {
                    form.SetAttribute("target", "_self");
                }
            

                if (murl.StartsWith("http://www.taobao.com"))
                {  
                    Console.WriteLine("打开主页成功");
                    webBrowser1.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);  
                    try
                    {

                        HtmlElement searchForm = webBrowser1.Document.All["q"];
                        searchForm.InvokeMember("mousedown");
                        searchForm.InvokeMember("mouseup"); 
                        searchForm.InvokeMember("click");
                        searchForm.Focus();
                        
                       searchForm.SetAttribute("value", kw); 
                        //for (int i = 0; i < kw.Length; i++)
                        //{
                        //    searchForm.Focus();
                        //    SendKeys.Send(kw.Substring(i, 1));
                        //    SendKeys.Flush();
                        //    Console.WriteLine(kw.Substring(i, 1) + ":" + i.ToString());
                        //    delayTimeM(rd.Next(300, 1000));
                        //    // System.Threading.Thread.Sleep(rd.Next(100,1500));
                        //} 
                        HtmlElement searchButton = webBrowser1.Document.GetElementsByTagName("button")[0];
                        webBrowser1.DocumentCompleted+= new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);  
                        searchButton.InvokeMember("mousedown");
                       
                        searchButton.InvokeMember("mouseup");
                          searchButton.InvokeMember("click");
                    }
                    catch { }
                    Console.WriteLine("输入 【"+kw+"】并点击搜索按钮成功");
                    timeout.Interval = 35000;
                    uncompleted = true;
                }
                else if (murl.StartsWith("http://s.taobao.com") || murl.StartsWith("http://re.taobao.com")||murl.StartsWith("http://s.m.taobao.com"))
                {
                    try
                    {
                        Console.WriteLine(kw+" 搜索结果" );
                        webBrowser1.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);
                    }
                    catch { }
                    finally
                    {
                        clickt.Interval = rd.Next(13000, 23000);
                        clickt.Enabled = true;
                    }

                }
                else if(murl.StartsWith("http://m.taobao.com")) {
                    webBrowser1.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);

                   // delayTime(20);
                    //HtmlElement searchForm = webBrowser1.Document.GetElementById("J_indexform").GetElementsByTagName("input")[0];
                    //searchForm.Focus();
                    //searchForm.InvokeMember("click");
                   
                    //searchForm.SetAttribute("value", kw);
                   //HtmlElement searchButton = webBrowser1.Document.GetElementById("J_indexform").GetElementsByTagName("input")[1];
                  // MessageBox.Show(searchButton.InnerHtml );
                   //webBrowser1.Document.GetElementById("J_indexform").InvokeMember("submit");
                    //searchButton.InvokeMember("click");
                    //searchButton.InvokeMember("onmouseout");
                    //searchForm.InvokeMember("submit");
                }

            }
            catch (Exception o) {Console.WriteLine(o.ToString()); }
           // isUserAgentSet = false;
        }


        private void delayTime(double secend)
        {
            DateTime tempTime = DateTime.Now;
            while (tempTime.AddSeconds(secend).CompareTo(DateTime.Now) > 0)
            {
               // Console.WriteLine(tempTime.AddSeconds(secend).CompareTo(DateTime.Now).ToString());
                Application.DoEvents();
            }
        }

        private void delayTimeM(double secend)
        {
            DateTime tempTime = DateTime.Now;
            while (tempTime.AddMilliseconds(secend).CompareTo(DateTime.Now) > 0)
            {
                // Console.WriteLine(tempTime.AddSeconds(secend).CompareTo(DateTime.Now).ToString());
                Application.DoEvents();
            }
        }

        private void clickt_Tick(object sender, EventArgs e)
        {
         
            clickme();
        }
        private void timeouttick(object sender, EventArgs e)
        {
            Console.WriteLine(uncompleted.ToString());
            if (uncompleted)
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter("taskok.txt", false))
                    {
                        sw.WriteLine("error");
                    }
                    Console.WriteLine(doctitle + "error3");
                }
                catch { }
                Application.Exit();
            }
        }
           public static int findPosX(HtmlElement obj)
    {
        int curleft = 0;
       // MessageBox.Show(obj.OffsetRectangle.X.ToString());
          //  while (null!=obj.Parent  )
          //  {
             curleft = obj.OffsetRectangle.X;
              //  obj = obj.Parent;
          //  }
        

        return curleft;
    }

    public static int findPosY(HtmlElement obj)
    {
        int curtop = 0;
     //   MessageBox.Show(obj.OffsetRectangle.Y.ToString());
     //   while (null !=obj.Parent )
       //     {
                curtop  = obj.OffsetRectangle.Y;
          //      obj = obj.Parent;
         //   }
       

        return curtop;
    }

        private void clickme()
        {
            try
            {
              
                clickt.Enabled = false;
       
               int x = 247, y = 552; 
               
                IntPtr handle = webBrowser1.Handle;
                StringBuilder className = new StringBuilder(100);
                while (className.ToString() != "Internet Explorer_Server") // The class control for the browser 
                {
                    handle = GetWindow(handle, 5); // Get a handle to the child window 
                    GetClassName(handle, className, className.Capacity);
                } 
                IntPtr lParam = (IntPtr)((y << 16) | x); // The coordinates 
                IntPtr wParam = IntPtr.Zero; // Additional parameters for the click (e.g. Ctrl) 
                const uint downCode = 0x201; // Left click down code 
                const uint upCode = 0x202; // Left click up code  
                 
                SendMessage(handle, downCode, wParam, lParam); // Mouse button down 
                SendMessage(handle, upCode, wParam, lParam); // Mouse button up 

                delayTime(3);
                x = 135; y = 488; 
                 lParam = (IntPtr)((y << 16) | x); 
                  SendMessage(handle, downCode, wParam, lParam); // Mouse button down 
                SendMessage(handle, upCode, wParam, lParam); // Mouse button up 


                int gocl = 0;
                //int divcc = 0;
                ArrayList div_h3 = new ArrayList();


                foreach (HtmlElement divc in webBrowser1.Document.GetElementById("page").GetElementsByTagName("div"))
                {

                    //divcc++;
                    //int myrdb = rd.Next(0, 2);
                    //int myrd = rd.Next(0, 3);

                    if (null != divc.InnerHtml && divc.GetElementsByTagName("h3").Count < 2 && divc.GetElementsByTagName("h3").Count > 0)
                    {

                        if (divc.InnerHtml.IndexOf("http://re.taobao.com") != -1)
                        { }
                        else
                            div_h3.Add(divc);
                    }
                }


                if (div_h3.Count != 0)
                {


                    HtmlElement a = (HtmlElement)div_h3[rd.Next(0, div_h3.Count)];

                    //if (divc.GetElementsByTagName("a").Count > 5 && divc.GetElementsByTagName("a")[5].InnerHtml.IndexOf(workdj) != -1)
                    //{
                    //HtmlElement  
                    try
                    {

                        a = a.GetElementsByTagName("h3")[0].GetElementsByTagName("a")[0];

                        a.InvokeMember("mousedown");
                        a.InvokeMember("mouseup");
                        a.InvokeMember("click");


                        gocl = 1;
                    }
                    catch { gocl = 0; }
                    //}
                    //else if (myrdb == 1)
                    //{
                    //    HtmlElement a = divc.GetElementsByTagName("h3")[0].GetElementsByTagName("a")[0];
                    //    a.InvokeMember("onmousedown");
                    //    a.InvokeMember("click");
                    //    a.InvokeMember("onmouseout");
                    //    gocl = 1;

                    //} 

                    if (gocl == 0)
                    {
                        try
                        {
                            //HtmlElement
                            a = webBrowser1.Document.GetElementById("page").GetElementsByTagName("h3")[0].GetElementsByTagName("a")[0];
                            a.InvokeMember("mousedown");

                            a.InvokeMember("mouseup");
                            a.InvokeMember("click");

                        }
                        catch { }
                    }
                }

            

                Console.WriteLine("点击成功"); 
                delayTime(rd.Next(5,8));
            }
            catch (Exception o) { Console.WriteLine(o.ToString()); }
            finally {
                

                while(true )
                {
                    try
                    {
                      
                        using (StreamWriter sw = new StreamWriter("taskok.txt", false))
                        {
                            sw.WriteLine(time_id);
                        }
                       
                        Console.WriteLine("写日志成功");
                        break;
                    }
                    catch(Exception o) {
                        Console.WriteLine(o.ToString());
                        System.Threading.Thread.Sleep(2000);
                    }

                }

            }
        }




      


    }
}
